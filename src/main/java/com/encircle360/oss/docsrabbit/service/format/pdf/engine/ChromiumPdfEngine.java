package com.encircle360.oss.docsrabbit.service.format.pdf.engine;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Base64;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.encircle360.oss.docsrabbit.service.format.pdf.PdfEngine;
import com.github.kklisura.cdt.launch.ChromeArguments;
import com.github.kklisura.cdt.launch.ChromeLauncher;
import com.github.kklisura.cdt.protocol.commands.Page;
import com.github.kklisura.cdt.protocol.types.page.PrintToPDF;
import com.github.kklisura.cdt.protocol.types.page.PrintToPDFTransferMode;
import com.github.kklisura.cdt.services.ChromeDevToolsService;
import com.github.kklisura.cdt.services.ChromeService;
import com.github.kklisura.cdt.services.types.ChromeTab;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ChromiumPdfEngine implements PdfEngine {

    @Value("${docsrabbit.engine.chromium.name}")
    private String EXECUTABLE;

    @Value("${docsrabbit.engine.chromium.timeout}")
    private Integer TIMEOUT;

    private final ChromeLauncher launcher;

    private static ChromeService chromeInstance;

    public ChromiumPdfEngine() {
        launcher = new ChromeLauncher();
    }

    @Override
    public String executable() {
        return EXECUTABLE;
    }

    @Override
    public List<String> parameters() {
        return List.of();
    }

    @Override
    public byte[] generatePDFDocument(final String content,
                                      final String headerTemplate,
                                      final String footerTemplate,
                                      final boolean landscape) throws IOException {
        String inputFile = this.writeTempHtmlFile(content).getAbsolutePath();
        String outputFile = "/tmp/"
            .concat(String.valueOf(Instant.now().getEpochSecond()))
            .concat("-")
            .concat(UUID.randomUUID().toString())
            .concat("-page.pdf");

        // timeout is used (if chromium has issues it will be restarted)
        CompletableFuture<Boolean> future = CompletableFuture.supplyAsync(() -> {
            this.execute(inputFile, outputFile, headerTemplate, footerTemplate, landscape);
            return true;
        }).orTimeout(TIMEOUT, TimeUnit.MINUTES);

        try {
            future.join();
        } catch (Exception e) {
            log.error("Failed rendering PDF", e);
            launcher.close();
        }

        byte[] bytes = this.readFile(outputFile);
        this.cleanup(inputFile, outputFile);

        return bytes;
    }

    private synchronized ChromeService getChrome() {
        if (!launcher.isAlive()) {
            chromeInstance = launcher.launch(Path.of(executable()), defaultArguments());
        }
        return chromeInstance;
    }

    @Override
    public void execute(String inputFile, String outputFile, String headerTemplate, String footerTemplate, boolean landscape) {
        ChromeService chrome = getChrome();
        ChromeTab tab = chrome.createTab();
        ChromeDevToolsService devTools = chrome.createDevToolsService(tab);

        Page page = devTools.getPage();
        page.enable();
        page.navigate("file://" + inputFile);

        page.onLoadEventFired(event -> {
            Boolean displayHeaderFooter = headerTemplate != null || footerTemplate != null;
            Boolean printBackground = true;
            Double scale = 1d;
            Double paperWidth = 8.27d; // A4 paper format
            Double paperHeight = 11.7d; // A4 paper format
            Double marginTop = 0.5d;
            Double marginBottom = 0.5d;
            Double marginLeft = 0.5d;
            Double marginRight = 0.5d;
            String pageRanges = "";
            Boolean ignoreInvalidPageRanges = false;
            Boolean preferCSSPageSize = true;
            PrintToPDFTransferMode mode = PrintToPDFTransferMode.RETURN_AS_BASE_64;

            PrintToPDF printToPDF = page.printToPDF(
                landscape,
                displayHeaderFooter,
                printBackground,
                scale,
                paperWidth,
                paperHeight,
                marginTop,
                marginBottom,
                marginLeft,
                marginRight,
                pageRanges,
                ignoreInvalidPageRanges,
                headerTemplate,
                footerTemplate,
                preferCSSPageSize,
                mode
            );

            byte[] bytes = Base64.getDecoder().decode(printToPDF.getData());
            try {
                Files.write(Path.of(outputFile), bytes);
            } catch (IOException e) {
                log.error("Writing file failed", e);
            }
            devTools.close();
        });

        devTools.waitUntilClosed();
        chrome.closeTab(tab);
    }

    private byte[] readFile(String outputFile) throws IOException {
        Path path = Path.of(outputFile);

        return Files.readAllBytes(path);
    }

    private ChromeArguments defaultArguments() {
        return ChromeArguments
            .builder()
            .headless(true)
            .disableGpu()
            .disableExtensions()
            .userDataDir("/tmp")
            .additionalArguments("no-sandbox", true)
            .additionalArguments("crash-dumps-dir", "/tmp")
            .build();
    }
}
