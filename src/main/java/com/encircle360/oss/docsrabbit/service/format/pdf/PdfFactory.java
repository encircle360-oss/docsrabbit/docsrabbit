package com.encircle360.oss.docsrabbit.service.format.pdf;

import com.encircle360.oss.docsrabbit.dto.render.RenderEngineDTO;
import com.encircle360.oss.docsrabbit.service.format.pdf.engine.ChromiumPdfEngine;
import com.encircle360.oss.docsrabbit.service.format.pdf.engine.WkhtmlToPdfEngine;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class PdfFactory {

    @Value("${docsrabbit.pdf.default.engine}")
    private RenderEngineDTO defaultEngine;

    private final ChromiumPdfEngine chromiumPdfEngine;
    private final WkhtmlToPdfEngine wkhtmlToPdfService;

    public String renderPdf(final String content,
                            final String header,
                            final String footer,
                            final boolean landscape,
                            final RenderEngineDTO engine) throws Exception {
        PdfEngine pdfEngine = this.mapEngine(engine);

        return pdfEngine.generateBase64PDFDocument(content, header, footer, landscape);
    }

    private PdfEngine mapEngine(RenderEngineDTO engine) {
        PdfEngine pdfEngine = null;
        if (engine == null || RenderEngineDTO.DEFAULT.equals(engine)) {
            engine = defaultEngine;
        }

        if (RenderEngineDTO.WKHTML_TO_PDF.equals(engine)) {
            pdfEngine = this.wkhtmlToPdfService;
        } else if (RenderEngineDTO.CHROMIUM.equals(engine)) {
            pdfEngine = this.chromiumPdfEngine;
        }

        log.debug("Using PDF Engine: {}", pdfEngine);

        return pdfEngine;
    }
}
