package com.encircle360.oss.docsrabbit.service.format.pdf;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.encircle360.oss.docsrabbit.util.IOUtils;

public interface PdfEngine {

    String executable();

    List<String> parameters();

    byte[] generatePDFDocument(String htmlContent, String htmlHeader, String htmlFooter, boolean landscape) throws Exception;

    default String generateBase64PDFDocument(String htmlContent, String htmlHeader, String htmlFooter, boolean landscape) throws Exception {
        byte[] pdfDocument = this.generatePDFDocument(htmlContent, htmlHeader, htmlFooter, landscape);

        return IOUtils.toBase64(pdfDocument);
    }

    default void execute(final String inputFile,
                         final String outputFile,
                         final String headerTemplate,
                         final String footerTemplate,
                         final boolean landscape) {
        List<String> commands = this.prepareCommand(inputFile, outputFile, headerTemplate, footerTemplate);

        try {
            Process p = new ProcessBuilder(commands).start();
            p.waitFor();
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException("Failed while execute rendering. " + e.getMessage());
        }
    }

    default File writeTempHtmlFile(String content) throws IOException {
        File directory = Path.of("/tmp").toFile();
        File tmpFile = File.createTempFile("tmp_", ".html", directory);
        tmpFile.deleteOnExit();
        FileWriter writer = new FileWriter(tmpFile, Charset.defaultCharset());
        writer.write(content);
        writer.close();

        return tmpFile;
    }

    private List<String> prepareCommand(String inputFile, String outputFile, String headerTemplate, String footerTemplate) {
        String command = this.executable();
        List<String> parameters = this.parameters()
            .stream()
            .map(parameter -> {
                parameter = parameter.replace("{{inputFile}}", inputFile)
                    .replace("{{outputFile}}", outputFile);
                if (headerTemplate != null) {
                    parameter = parameter.replace("{{headerTemplate}}", headerTemplate);
                }
                if (footerTemplate != null) {
                    parameter = parameter.replace("{{footerTemplate}}", footerTemplate);
                }
                return parameter;
            })
            .collect(Collectors.toList());

        List<String> commands = new ArrayList<>();
        commands.add(command);
        commands.addAll(parameters);

        return commands;
    }

    default void cleanup(String inputFile, String outputFile) {
        Path.of(inputFile).toFile().delete();
        Path.of(outputFile).toFile().delete();
    }
}
