package com.encircle360.oss.docsrabbit.service.template;

import com.encircle360.oss.docsrabbit.config.MongoDbConfig;
import com.encircle360.oss.docsrabbit.model.Template;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
@Profile("!" + MongoDbConfig.PROFILE)
public class FileTemplateLoader extends AbstractTemplateLoader {

    @PostConstruct
    public void logTemplates() throws IOException {
        Resource folder =  new PathResource("/resources/templates/");
        String fileName = folder.getFile().getPath();
        Path path = Path.of(fileName);

        if (!path.toFile().exists()) {
            log.debug("/resources/templates/ does not exists!");
            return;
        }

        Files.walk(path)
                .collect(Collectors.toList())
                .stream()
                .filter(file -> file.getFileName().toString().contains("."))
                .forEach(file -> log.info("Template File {}", file.toAbsolutePath()));
    }

    @Override
    public Template loadTemplate(String templateId) {
        return super.loadFromFiles(templateId);
    }
}
