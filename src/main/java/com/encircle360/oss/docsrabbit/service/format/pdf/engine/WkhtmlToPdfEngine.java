package com.encircle360.oss.docsrabbit.service.format.pdf.engine;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.encircle360.oss.docsrabbit.service.format.pdf.PdfEngine;
import com.github.jhonnymertz.wkhtmltopdf.wrapper.Pdf;
import com.github.jhonnymertz.wkhtmltopdf.wrapper.configurations.WrapperConfig;
import com.github.jhonnymertz.wkhtmltopdf.wrapper.configurations.XvfbConfig;
import com.github.jhonnymertz.wkhtmltopdf.wrapper.params.Param;

@Service
public class WkhtmlToPdfEngine implements PdfEngine {

    @Override
    public String executable() {
        return "wkhtmltopdf";
    }

    @Override
    public List<String> parameters() {
        return List.of();
    }

    @Override
    public byte[] generatePDFDocument(final String htmlContent,
                                      final String htmlHeader,
                                      final String htmlFooter,
                                      final boolean landscape) throws IOException, InterruptedException {
        File htmlHeaderFile = null;
        File htmlFooterFile = null;
        Pdf pdf = new Pdf(this.getWkhtmlToPdfWrapperConfig());
        pdf.addParam(new Param("--encoding", Charset.defaultCharset().name()));
        pdf.addParam(new Param("--print-media-type"));

        if (landscape) {
            pdf.addParam(new Param("--orientation", "landscape"));
        }

        // add header if given
        if (htmlHeader != null && !htmlHeader.isEmpty()) {
            htmlHeaderFile = this.writeTempHtmlFile(htmlHeader);
            pdf.addParam(new Param("--header-html", htmlHeaderFile.getAbsolutePath()));
        }

        if (htmlFooter != null && !htmlFooter.isEmpty()) {
            htmlFooterFile = this.writeTempHtmlFile(htmlFooter);
            pdf.addParam(new Param("--footer-html", htmlFooterFile.getAbsolutePath()));
        }

        pdf.addPageFromString(htmlContent);
        byte[] generatedPdfAsBytes = pdf.getPDF();

        // cleanup if needed
        if (htmlHeaderFile != null) {
            htmlHeaderFile.delete();
        }

        if (htmlFooterFile != null) {
            htmlFooterFile.delete();
        }

        return generatedPdfAsBytes;
    }

    private WrapperConfig getWkhtmlToPdfWrapperConfig() {
        WrapperConfig wc = new WrapperConfig(WrapperConfig.findExecutable());
        if (!isRunningInsideDocker()) {
            return wc;
        }

        XvfbConfig xc = new XvfbConfig();
        xc.addParams(new Param("--auto-servernum"));
        wc.setXvfbConfig(xc);

        return wc;
    }

    private boolean isRunningInsideDocker() {
        try (Stream<String> stream = Files.lines(Paths.get("/proc/1/cgroup"))) {
            return stream.anyMatch(line -> line.contains("/docker"));
        } catch (IOException e) {
            return false;
        }
    }
}
