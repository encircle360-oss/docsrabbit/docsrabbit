package com.encircle360.oss.docsrabbit.dto.render;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;

@Getter
@Schema(name = "RenderEngine", description = "Defines the used render engine")
public enum RenderEngineDTO {
    DEFAULT("Default"),
    CHROMIUM("Chromium"),
    WKHTML_TO_PDF("WkhtmlToPdf");

    private final String value;

    RenderEngineDTO(String engine) {
        this.value = engine;
    }
}
