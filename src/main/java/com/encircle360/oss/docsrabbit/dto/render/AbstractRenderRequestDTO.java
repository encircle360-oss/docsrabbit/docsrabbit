package com.encircle360.oss.docsrabbit.dto.render;

import java.util.HashMap;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.JsonNode;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "AbstractRenderRequest")
public abstract class AbstractRenderRequestDTO {
    @NotNull
    @Schema(name = "format", description = "The format in which the template should be rendered")
    private RenderFormatDTO format;

    @Builder.Default
    @Schema(name = "engine", description = "The engine which is used for rendering, otherwise the default will be used.")
    private RenderEngineDTO engine = RenderEngineDTO.DEFAULT;

    @Schema(name = "model", description = "Map with all attributes needed for rendering the template")
    private HashMap<String, JsonNode> model;

    @Schema(name = "locale", description = "The locale which should be used for rendering, only needed when using filesystem", example = "de")
    private String locale;

    @Schema(description = "Set to true if you want to render a page in landscape format.")
    private boolean landscape;
}
