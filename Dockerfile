FROM ubuntu:18.04
# keep ubuntu:18.04 - chromium is only avaible with install via snap, but doesn't work under docker!

ENV TZ=Europe/Berlin
ENV DEBIAN_FRONTEND=noninteractive
ARG TARGETPLATFORM

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get -y update
RUN apt-get -y install chromium-codecs-ffmpeg="65.0.3325.181-0ubuntu1"
RUN apt-get -y install chromium-codecs-ffmpeg-extra="65.0.3325.181-0ubuntu1"
RUN apt-get -y install chromium-browser="65.0.3325.181-0ubuntu1"
RUN apt-get -y install apt-transport-https
RUN apt-get -y install apt-utils
RUN apt-get -y install tzdata
RUN apt-get -y install openssl
RUN apt-get -y install xvfb
RUN apt-get -y install wget
RUN apt-get -y install xz-utils
RUN apt-get -y install bzip2
RUN apt-get -y install fontconfig
RUN apt-get -y install libfreetype6
RUN apt-get -y install libjpeg-turbo8
RUN apt-get -y install libpng16-16
RUN apt-get -y install libx11-6
RUN apt-get -y install libxcb1
RUN apt-get -y install libxext6
RUN apt-get -y install libxrender1
RUN apt-get -y install xfonts-75dpi
RUN apt-get -y install xfonts-base
RUN apt-get -y install libreoffice
RUN apt-get -y install python
RUN apt-get -y install unoconv

RUN apt-get -y install openjdk-17-jdk

ARG wkhtml_amd64_URL=https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.bionic_amd64.deb
ARG wkhtml_arm64_URL=https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.bionic_arm64.deb

RUN if [ "${TARGETPLATFORM}" = "linux/arm64" ];  \
    then wget -O wkhtmlpdf.deb $wkhtml_arm64_URL; \
    else wget -O wkhtmlpdf.deb $wkhtml_amd64_URL; \
    fi

RUN dpkg -i wkhtmlpdf.deb

ADD /build/libs/*.jar /docsrabbit.jar

ENTRYPOINT ["java","-Duser.language=en-US", "-Dfile.encoding=UTF-8", "-Djava.security.egd=file:/dev/./urandom","-jar","/docsrabbit.jar"]
