#!/usr/bin/env bash

chromium-browser --headless --no-sandbox --disable-gpu --disable-extensions --print-to-pdf-no-header --disk-cache-dir=/tmp --user-data-dir=/tmp --crash-dumps-dir=/tmp --headerTemplate='' --footerTemplate='' --print-to-pdf=page.pdf page.html